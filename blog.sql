-- Active: 1673947610856@@127.0.0.1@3306@blog
DROP TABLE IF EXISTS article_tag;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS tag;
CREATE TABLE article (
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    date DATE,
    author VARCHAR(100),
    cover VARCHAR(255),
    text VARCHAR(2000),
    spotify VARCHAR(255)
    );

CREATE TABLE tag (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (50)
);

CREATE TABLE article_tag (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_article INT,
    id_tag INT
);

INSERT INTO tag (name) VALUES ('grunge');
INSERT INTO article (title, date, author, cover, text, spotify) VALUES 
('Hole - Live Through This', '2022-02-08', 'Laura', 'https://m.media-amazon.com/images/I/81PhSgamqPL._SL1500_.jpg', 'Texte texte texte', 'https://open.spotify.com/embed/album/2Rwf2nPYZQ9aIe4QXACTC7?si=wEOfsCuSTDWA2V1wjh6lOw')
;

INSERT INTO article (title, date, author, cover, text, spotify) VALUES 
('L7 - Smell The Magic', '2022-02-08', 'Laura', 'https://m.media-amazon.com/images/I/81Zbkf2XJ6L._SX425_.jpg', 'textttttttttte texte', 'https://open.spotify.com/embed/album/0VgXvWzdF93KHuNdzzSgaB?si=g2MywXSDTMKR1VjCYhJDCQ')
;