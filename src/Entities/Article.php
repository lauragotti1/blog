<?php

namespace App\Entities;
use DateTime;

class Article {
    private ?int $id;
    private ?string $title;
    private ?DateTime $date;
    private ?string $author;
    private ?string $cover;
    private ?string $text;
    private ?string $spotify;


    /**
     * @param int|null $id
     * @param string|null $title
     * @param DateTime|null $date
     * @param string|null $author
     * @param string|null $cover
     * @param string|null $text
     * @param string|null $spotify
     */
    public function __construct(?string $title, ?DateTime $date, ?string $author, ?string $cover, ?string $text, ?string $spotify, ?int $id = null) {
    	$this->id = $id;
    	$this->title = $title;
    	$this->date = $date;
    	$this->author = $author;
    	$this->cover = $cover;
    	$this->text = $text;
    	$this->spotify = $spotify;
    }

    

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param string|null $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return DateTime|null
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime|null $date 
	 * @return self
	 */
	public function setDate(?DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getAuthor(): ?string {
		return $this->author;
	}
	
	/**
	 * @param string|null $author 
	 * @return self
	 */
	public function setAuthor(?string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getCover(): ?string {
		return $this->cover;
	}
	
	/**
	 * @param string|null $cover 
	 * @return self
	 */
	public function setCover(?string $cover): self {
		$this->cover = $cover;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getText(): ?string {
		return $this->text;
	}
	
	/**
	 * @param string|null $text 
	 * @return self
	 */
	public function setText(?string $text): self {
		$this->text = $text;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getSpotify(): ?string {
		return $this->spotify;
	}
	
	/**
	 * @param string|null $spotify 
	 * @return self
	 */
	public function setSpotify(?string $spotify): self {
		$this->spotify = $spotify;
		return $this;
	}
}