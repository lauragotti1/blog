<?php
namespace App\Repository;

use App\Entities\Article;
use DateTime;
use PDO;


class ArticleRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    private function sqlToArticle(array $line):Article {
        $date = null;
        if(isset($line['date'])){
            $date = new DateTime($line['date']);
        }
        return new Article($line['title'], $date, $line['author'], $line['cover'], $line['text'], $line['spotify'], $line['id']);
    }

    public function findAll():array {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article');
        $statement->execute();
        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;
    }

    public function findById(int $id):?Article {
        $statement = $this->connection->prepare('SELECT * FROM article WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToArticle ($result);
        }
        return null;
    }

    public function persist(Article $article) {
        $statement = $this->connection->prepare("INSERT INTO article (title, date, author, cover, text, spotify) VALUES (:title, :date, :author, :cover, :text, :spotify)");
        $statement->execute([
            'title' => $article->getTitle(),
            'date' => $article->getDate()->format('Y-m-d'),
            'author' => $article->getAuthor(),
            'cover' => $article->getCover(),
            'text' => $article->getText(),
            'spotify' => $article->getSpotify()
        ]);

        $article->setId($this->connection->lastInsertId());
        
    }

    public function update(Article $article):void{
        $statement = $this->connection->prepare('UPDATE article SET title=:title, date=:date, author=:author, cover=:cover, text=:text, spotify=:spotify WHERE id=:id');
        $statement->execute([
            'title' => $article->getTitle(),
            'date' => $article->getDate()->format('Y-m-d'),
            'author' => $article->getAuthor(),
            'cover' => $article->getCover(),
            'text' => $article->getText(),
            'spotify' => $article->getSpotify(),
            'id'=> $article->getId(),
        ]);

    }

    public function deleteById(int $id) {
        $statement = $this->connection->prepare('DELETE FROM article WHERE id = :id');
        $statement->execute([
            'id' => $id
        ]);
    }

    public function searchTitle($title) {
        $articles = [];
        $statement = $this->connection->prepare('SELECT * FROM article WHERE title LIKE :title');
        $statement->execute([
            'title'=> '%'.$title.'%'
        ]);
        $results = $statement->fetchAll();
        foreach($results as $item) {
            $articles[] = $this->sqlToArticle($item);
        }
        return $articles;
    }


}